/************************************************************************
 VisualOn Proprietary
 Copyright (c) 2018, VisualOn Incorporated. All Rights Reserved
 
 VisualOn, Inc., USA
 
 All data and information contained in or disclosed by this document are
 confidential and proprietary information of VisualOn, and all rights
 therein are expressly reserved. By accepting this material, the
 recipient agrees that this material and the information contained
 therein are held in confidence and in trust. The material may only be
 used and/or disclosed as authorized in a license agreement controlling
 such use and disclosure.
 ************************************************************************/

#import <Foundation/NSObject.h>

/**
 * A VOPlayerConfig be used for overwrite the setting of player's API.
 * Player will use the configuration of VOPlayerConfig as first priority.
 * Only be used for support some analytics export at present.
 */
@protocol VOPlayerConfig <NSObject>

/**
 * Parse the configuration of Json string, and overwrite the exist configuration.
 * And will keep the configuration which not in the Json string.
 *
 * @param   jsonString   [in] The json string.
 *
 * @return  BOOL         Return YES if success
 */
- (BOOL)parseJson:(nonnull NSString *)jsonString;

/**
 * Set configuration by key.
 * @param   key          [in] configuration key.
 * @param   value        [in] configuration value.
 *
 */
- (void)setValue:(nullable id)value forKey:(nonnull NSString *)key;

/**
 * Get configuration by key.
 * @param   key          [in] configuration key.
 *
 * @return  id           The configuration value
 */
- (nullable id)valueForKey:(nonnull NSString *)key;

@end
