Pod::Spec.new do |spec|

  #  Spec Metadata
  spec.name         = 'YouboraVisualOnAnalyticsAdapter'
  spec.version      = '6.6.0'

  spec.summary      = 'Adapter to use YouboraLib on VisualOnAnalytics player.'

  spec.description  = <<-DESC
                          YouboraVisualOnAnalyticsAdapter is an adapter used for VisualOnAnalytics player.
                      DESC

  spec.homepage     = 'https://documentation.npaw.com/npaw'


  #  Spec License
  spec.license      = { :type => 'MIT', :file => 'LICENSE.md' }


  # Author Metadata
  spec.author             = { 'NPAW' => 'support@nicepeopleatwork.com' }

  # Platform
  spec.ios.deployment_target = '10.0'

  spec.swift_version = '4.0', '4.1', '4.2', '4.3', '5.0', '5.1', '5.2', '5.3'


  # Source Location
  spec.source       = { :git => 'https://bitbucket.org/npaw/visualonanalytics-adapter-ios.git', :tag => spec.version }


  # Source Code
  spec.source_files  = 'YouboraVisualOnAnalyticsAdapter/**/*.{h,m,swift}'

  # Project Settings
  spec.pod_target_xcconfig = { 
    'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORAVISUALONADAPTER_VERSION=' + spec.version.to_s,
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  spec.user_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  
  # Project Linking
  spec.vendored_frameworks = 'VisualOnOSMP.xcframework'

  # Dependency
  spec.dependency 'YouboraLib', '~> 6.5'

end
