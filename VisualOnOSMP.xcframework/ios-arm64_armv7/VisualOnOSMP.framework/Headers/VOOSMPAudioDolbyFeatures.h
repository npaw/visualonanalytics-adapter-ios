/************************************************************************
 VisualOn Proprietary
 Copyright (c) 2018, VisualOn Incorporated. All Rights Reserved
 
 VisualOn, Inc., USA
 
 All data and information contained in or disclosed by this document are
 confidential and proprietary information of VisualOn, and all rights
 therein are expressly reserved. By accepting this material, the
 recipient agrees that this material and the information contained
 therein are held in confidence and in trust. The material may only be
 used and/or disclosed as authorized in a license agreement controlling
 such use and disclosure.
 ************************************************************************/

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, VOOSMPDolbyEndPoint)
{
    VOOSMPDolbyEndPointStereoSpeaker    = 1,
    VOOSMPDolbyEndPointStereoHeadPhone  = 2 // Defalut
};

typedef NS_ENUM(NSInteger, VOOSMPDolbyProgram)
{
    VOOSMPDolbyProgramDisable = 0, // Default
    VOOSMPDolbyProgram1       = 1,
    VOOSMPDolbyProgram2       = 2,
    VOOSMPDolbyProgram3       = 3
};

typedef NS_ENUM(NSInteger, VOOSMPDolbyInputMode)
{
    VOOSMPDolbyInputModeSingleInput = 0, // Default
    VOOSMPDolbyInputModeDualInput   = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface VOOSMPAudioDolbyFeatures : NSObject

/**
 * Option to set audio output.
 */
@property (nonatomic, assign)VOOSMPDolbyEndPoint endPoint;

/**
 * Turns on/off postprocessing. Default:NO.
 */
@property (nonatomic, assign)BOOL postprocessing;

/**
 * The level of dialogue enhancement only take effect when postprocessing is turned on.
 *
 * Available range : 0-12. Default:NO.
 */
@property (nonatomic, assign)int dialogueEnhancementLevel;

/**
 * Using dual decoding and mixing or not. Default:NO.
 *
 * Yes for using decoding and mixing and No for using decode main only.
 */
@property (nonatomic, assign)BOOL dualDecodingAndMixing;

/**
 * Mixing Balance. Default:0.
 *
 * <ul> Available value:
 * <li> -32    : Associated program fully muted.
 * <li> -31~-1 : Favor main program.
 * <li> 0      : Neutral.
 * <li> 1-31   : Favor associated program.
 * <li> 32     : Main program fully muted.
 * * </ul></p>
 */
@property (nonatomic, assign)int mixingBalance;

/**
 * Indicates whether there are one or two input bitstreams for dual decoding.
 */
@property (nonatomic, assign)VOOSMPDolbyInputMode inputMode;

/**
 * Selects the associated audio program substream to decode. Available for single-input dual decoding only. If the input mode is set to dual input, this control is not available.
  */
@property (nonatomic, assign)VOOSMPDolbyProgram program;

/**
 * Enable joint object coding.
 * Yes for setting the decoder to decode a Dolby Digital Plus Joint Object Coding bitstream.
 * No for setting the decoder to decode a Dolby Digital Plus (or Dolby Digital) bitstream.
 */
@property (nonatomic, assign)BOOL enableJOC;


@end

NS_ASSUME_NONNULL_END
