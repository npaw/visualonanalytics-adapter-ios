/************************************************************************
 VisualOn Proprietary
 Copyright (c) 2018, VisualOn Incorporated. All Rights Reserved
 
 VisualOn, Inc., USA
 
 All data and information contained in or disclosed by this document are
 confidential and proprietary information of VisualOn, and all rights
 therein are expressly reserved. By accepting this material, the
 recipient agrees that this material and the information contained
 therein are held in confidence and in trust. The material may only be
 used and/or disclosed as authorized in a license agreement controlling
 such use and disclosure.
 ************************************************************************/

#import <Foundation/NSObject.h>
#import "VOOSMPType.h"

NS_ASSUME_NONNULL_BEGIN
VOEXTERN NSString * const VOAnalyticsExport;
VOEXTERN NSString * const VOAnalyticsExportListener;
NS_ASSUME_NONNULL_END
