/************************************************************************
 VisualOn Proprietary
 Copyright (c) 2018, VisualOn Incorporated. All Rights Reserved
 
 VisualOn, Inc., USA
 
 All data and information contained in or disclosed by this document are
 confidential and proprietary information of VisualOn, and all rights
 therein are expressly reserved. By accepting this material, the
 recipient agrees that this material and the information contained
 therein are held in confidence and in trust. The material may only be
 used and/or disclosed as authorized in a license agreement controlling
 such use and disclosure.
 ************************************************************************/

#import "VOOSMPType.h"
#import "VOPlayerConfig.h"

EXPORT_API
@interface VOPlayerConfigImpl : NSObject <VOPlayerConfig>
{

}

/**
 * Init the object with another VOPlayerConfig.
 * {@link valueForKey} will find the value from self object at first, if the value not exist, will try to find the value from the playerConfig be set.
 *
 * @param   playerConfig   [in] The VOPlayerConfig.
 *
 * @return  id             Return object
 */
- (nonnull id) init:(nullable id<VOPlayerConfig>) playerConfig;

- (BOOL)parseJson:(nonnull NSString *)jsonString;

- (void)setValue:(nullable id)value forKey:(nonnull NSString *)key;
- (nullable id)valueForKey:(nonnull NSString *)key;

@end
