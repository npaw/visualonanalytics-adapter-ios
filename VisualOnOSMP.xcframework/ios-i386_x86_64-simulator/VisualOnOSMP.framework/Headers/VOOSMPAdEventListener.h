/************************************************************************
VisualOn Proprietary
Copyright (c) 2013, VisualOn Incorporated. All Rights Reserved

VisualOn, Inc., USA

All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, and all rights
therein are expressly reserved. By accepting this material, the
recipient agrees that this material and the information contained
therein are held in confidence and in trust. The material may only be
used and/or disclosed as authorized in a license agreement controlling
such use and disclosure.
************************************************************************/
#ifndef __OSMP_AD_EVENT_LISTENER_H__
#define __OSMP_AD_EVENT_LISTENER_H__

#include <vector>

#include "VOOSMPString.h"
#include "VOOSMPType.h"

/**
 * Enumeration of Ad event IDs.
 */
enum VO_OSMP_AD_EVENT_ID 
{
    /** Playback of an Ad break starts */
    VO_OSMP_AD_EVENT_AD_BREAK_START = 0, 

    /** Playback of an Ad break ends */
    VO_OSMP_AD_EVENT_AD_BREAK_END,

    /** Playback of an Ad pod starts */
    VO_OSMP_AD_EVENT_AD_POD_START,

    /** Playback of an Ad pod ends */
    VO_OSMP_AD_EVENT_AD_POD_END,

    /** Playback of an Ad starts */
    VO_OSMP_AD_EVENT_AD_START,

    /** Playback of an Ad ends */
    VO_OSMP_AD_EVENT_AD_END,

    /** Cue points */
    VO_OSMP_AD_EVENT_AD_CUE_POINTS,

    /** An event generated by an integrated SDK */
    VO_OSMP_AD_EVENT_SDK_EVENT,

    /** No-Ad VAST response */
    VO_OSMP_AD_EVENT_NO_AD_VAST,

    VO_OSMP_AD_EVENT_NUM
};

/**
 * Enumeration of type of information returned associated with an Ad callback 
 * event ID.
 */
enum VO_OSMP_AD_EVENT_INFO_TYPE 
{
    /** Ad break info */
    VO_OSMP_AD_EVENT_AD_BREAK_INFO = 0,

    /** Ad pod info */
    VO_OSMP_AD_EVENT_AD_POD_INFO,

    /** Ad info */
    VO_OSMP_AD_EVENT_AD_INFO,

    /** Cue points */
    VO_OSMP_AD_EVENT_AD_CUE_POINTS_INFO,

    /** Event generated by some SDK */
    VO_OSMP_AD_EVENT_SDK_EVENT_INFO,

    /** No-Ad VAST response info */
    VO_OSMP_AD_EVENT_NO_AD_VAST_INFO,

    VO_OSMP_AD_EVENT_INFO_NUM
};

/**
 * Enumeration of Ad type 
 */
enum VO_OSMP_AD_TYPE
{
    /** Linear Ad */
    VO_OSMP_AD_TYPE_LINEAR,

    /** Non-linear Ad */
    VO_OSMP_AD_TYPE_NON_LINEAR,

    VO_OSMP_AD_TYPE_NUM
};

/** Interface of info returned associated with an Ad event */
class VOOSMPAdEventInfo
{
    public: 
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

    protected:
        VOOSMPAdEventInfo() {}
        virtual ~VOOSMPAdEventInfo() {}
};

/** Interface of cue points */
class VOOSMPAdCuePointsInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Get Ad cue points */
        virtual std::vector<float> getAdCuePoints() const = 0;

    protected:
        VOOSMPAdCuePointsInfo() {}
        virtual ~VOOSMPAdCuePointsInfo() {} 
};

/** Interface of Ad Break info */
class VOOSMPAdBreakInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Check if the Ad break is a preroll */
        virtual bool isPreroll() const = 0;

        /** Check if the Ad break is a postroll */
        virtual bool isPostroll() const = 0;

        /** Check if the Ad break is a midroll */
        virtual bool isMidroll() const = 0;

        /** Check if the Ad break has an Ad pod */
        virtual bool hasAdPod() const = 0;

        /** Return the number of standalone Ads in the Ad break*/
        virtual unsigned getNumStandaloneAd() const = 0;

    protected:
        VOOSMPAdBreakInfo() {}
        virtual ~VOOSMPAdBreakInfo() {}
};

/** Interface of Ad Pod info */
class VOOSMPAdPodInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Return the number of Ads in the Ad pod */
        virtual unsigned getNumAd() const = 0;

    protected:
        VOOSMPAdPodInfo() {}
        virtual ~VOOSMPAdPodInfo() {}
};

/** Interface of Ad info */
class VOOSMPAdInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Get the type of Ad */
        virtual VO_OSMP_AD_TYPE getAdType() const = 0;

        /** Return Ad ID */
        virtual VOOSMPString getAdId() const = 0;

        /** Return Ad title */
        virtual VOOSMPString getAdTitle() const = 0;

        /** Return Creative ID */
        virtual VOOSMPString getCreativeId() const = 0;

        /** Return Ad duration */
        virtual long getAdDuration() const  = 0;

        /** Return the position (starting from 1) of Ad in the Ad pod. Return -1 if this Ad is not part of the Ad pod */
        virtual int getAdPodPos() const = 0;

        /** Check if the Ad can be skipped */
        virtual bool isSkippable() const = 0;

        /** Return the amount of time (in ms) before the Ad can be skipped */
        virtual long getSkipoffset() const = 0;

        /** Return the click-through URL of the Ad */
        virtual VOOSMPString getClickThroughUrl() const = 0;

        /** Check if this is a VPAID Ad */
        virtual bool isVPAID() const  = 0;

    protected:
        VOOSMPAdInfo() {}
        virtual ~VOOSMPAdInfo() {}
};

/** Interface of SDK event ad event info */
class VOOSMPSDKEventInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Return a JSON string with the message data */
        virtual const VOOSMPString& getJSONStr() const = 0;

    protected: 
        VOOSMPSDKEventInfo() {}
        virtual ~VOOSMPSDKEventInfo() {}
};

/** Interface of no-ad VAST ad event info */
class VOOSMPNoAdVASTInfo : public VOOSMPAdEventInfo
{
    public:
        /** Get the type of Ad event info */
        virtual VO_OSMP_AD_EVENT_INFO_TYPE getType() const = 0;

        /** Return a string with the VAST URL that returned no ad */
        virtual const VOOSMPString& getNoAdVASTUrl() const = 0;

    protected: 
        VOOSMPNoAdVASTInfo() {}
        virtual ~VOOSMPNoAdVASTInfo() {}
};


/**
 * Interface for Ad event listener. Implement this interface 
 * to receive event from the Ad manager.
 */
class VOOSMPAdEventListener
{
    public:
        VOOSMPAdEventListener() {}
        virtual ~VOOSMPAdEventListener() {}

        /**
         * Interface to receive events
         *
         * @param id    [in] Event type. Refer to {@link VO_OSMP_AD_EVENT_ID}.
         * @param info  [in] Information associated with the event. Refer to 
         *                   {@link VOOSMPAdEventInfo}
         *
         * @return {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful.
         */
        virtual VO_OSMP_RETURN_CODE onAdEvent(VO_OSMP_AD_EVENT_ID id, VOOSMPAdEventInfo* info) = 0;
};

#endif
