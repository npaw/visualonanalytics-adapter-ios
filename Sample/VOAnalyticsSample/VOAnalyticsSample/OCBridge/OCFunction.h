//
//  OCFunction.h
//  VOAnalyticsSample
//
//  Created by Elisabet Massó on 29/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

#ifndef OCFunction_h
#define OCFunction_h

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OCFunction : NSObject

+ (Rect)rect;

@end

NS_ASSUME_NONNULL_END


#endif /* OCFunction_h */
