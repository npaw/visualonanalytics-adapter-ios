/************************************************************************
VisualOn Proprietary
Copyright (c) 2013, VisualOn Incorporated. All Rights Reserved

VisualOn, Inc., USA

All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, and all rights
therein are expressly reserved. By accepting this material, the
recipient agrees that this material and the information contained
therein are held in confidence and in trust. The material may only be
used and/or disclosed as authorized in a license agreement controlling
such use and disclosure.
************************************************************************/
#ifndef __VOOSMP_AD_CALL_PARM_H__
#define __VOOSMP_AD_CALL_PARM_H__

#include "VOOSMPInterface.h"
#include "VOOSMPType.h"
#include "VOOSMPString.h"

enum VO_OSMP_AD_CALL_TYPE
{
    /** VAST preroll URL - Client-Stitched */
    VO_OSMP_AD_CALL_VAST = 0,

    /** VMAP URL - Client-Stitched */
    VO_OSMP_AD_CALL_VMAP,

    /** Ad.xml format URL - Client-Stitched */
    VO_OSMP_AD_CALL_ADSXML,

    /** Google IMA - Client-Stitched */
    VO_OSMP_AD_CALL_IMA,
    
    /** Google IMA - Dynamic Ad Insertion - Server-Stitched */
    VO_OSMP_AD_CALL_IMADAI,

    /** AdFlow does not intervene to trigger ad insertion. This is for 
        for server-stitched ads where ad related info comes from DASH 
        manifest / HLS playlist */
    VO_OSMP_AD_CALL_MANIFEST,
    VO_OSMP_AD_CALL_NONE = VO_OSMP_AD_CALL_MANIFEST,

    /** AWS MediaTailor - Server-Stitched */
    VO_OSMP_AD_CALL_MEDIATAILOR,

    VO_OSMP_AD_CALL_NUM
};

enum VO_OSMP_AD_OPEN_FLAG
{
    VO_ADSMANAGER_OPENFLAG_UNKNOWN         = 0x00000000,
    VO_ADSMANAGER_OPENFLAG_PID         	   = 0x00000001,
    VO_ADSMANAGER_OPENFLAG_URI_LIVE        = 0x00000002,
    VO_ADSMANAGER_OPENFLAG_URI_RAW         = 0x00000004,//Indicate the pSource will be VO_ADSMANAGER_SOURCE_PID* , pPID should be a URI of raw content
    VO_ADSMANAGER_OPENFLAG_URI_VAST        = 0x00000008,//Indicate the pSource will be VO_ADSMANAGER_SOURCE_PID* , pPID should be a URI of VAST file
    VO_ADSMANAGER_OPENFLAG_PID_LIVE        = 0x000000010,//Indicate the pSource will be VO_ADSMANAGER_SOURCE_PID* , pPID should be a ID can be converted to ...
    VO_ADSMANAGER_OPENFLAG_PID_DVR         = 0x000000020//Indicate the pSource will be VO_ADSMANAGER_SOURCE_PID* , pPID should be a ID can be converted to ...
};

class VOCommonPlayerAPI VOOSMPAdUISettings
{
    public:
        VOOSMPAdUISettings();
        ~VOOSMPAdUISettings();
        
        /**
         * Get content video view 
         *
         * @return the video view 
         */
        void* getVideoView() const;

        /**
         * Get Android player activity context
         *
         * @return activity context 
         */
        void* getContext();

        /**
         * Get iOS player view controller
         *
         * @return view controller 
         */
        void* getViewController();

        /**
         * Get VPAID UI component configuration settings
         *
         * @return VPAID UI component configuration settings in JSON  format
         *         string
         */
        const VOOSMPString& getVPAIDUIConfiguration() const;

        /**
         * Set content video view 
         *
         * @param  videoView [in] In VAST/VMAP or Android Google IMA flow, 
         *                        this is the UI view for content playback;
         *
         *                        In iOS Google IMA flow, this is the UI view
         *                        for ad playback
         * 
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setVideoView(void* videoView);

        /**
         * Set Android player activity context
         *
         * @param  context [in] Android player activity context
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setContext(void* context);

        /**
         * Set iOS player view controller
         *
         * @param  vc [in] iOS player view controller
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setViewController(void* vc);

        /**
         * Set VPAID UI configuration
         *
         * @param conf [in] JSON format string to enable/disable VPAID UI
         *                  components. 
         *
         *                  Valid keys are strings "pause", "mute", "skipAd" 
         *                  and "learnMore". The value of each key is a boolean
         *                  value indicating if the referred built-in UI component
         *                  is enabled during playback. By default all built-in
         *                  UI components are enabled. 
         *
         *                  For instance, the following configuration disables
         *                  "mute" and "learnMore" buttons
         *
         *                  { "mute":false, "learnMore":false }
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setVPAIDUIConfiguration(const char* conf);


    private:
        // UI view for content playback in VAST/VMAP/Android Google IMA flow
        // or UI view for ad playback in iOS Google IMA flow
        void* mVideoView;

        // for Android only 
        void* mContext; 
       
        // for iOS only
        void* mViewController; 

        // for VPAID UI configuration
        VOOSMPString mVPAIDUIConfiguration;
};

class VOCommonPlayerAPI VOOSMPAdCallParam
{
    public:
        VOOSMPAdCallParam();
        ~VOOSMPAdCallParam();

        /**
         * Get Ad call URL setting
         *
         * @return  Ad call URL
         */
        const VOOSMPString& getAdCallUrl() const;

        /**
         * Get Ad call URL type
         *
         * @return  Ad call URL type
         */
        VO_OSMP_AD_CALL_TYPE getAdCallType() const;

        /**
         * Get IU settings for VAST/VMAP or IMA flow
         *
         * @return  {@link VOOSMPAdUISettings} if successful
         */
        const VOOSMPAdUISettings& getUISettings() const;
    
        /**
         * Specify Ad call URL 
         *
         * @param  url [in] Ad call URL
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setAdCallUrl(const VOOSMPString&  url);

        /**
         * Specify the type of Ad call URL 
         *
         * @param  type [in] Ad call URL type. Refer to {@link VO_OSMP_AD_CALL_TYPE}
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */
        VO_OSMP_RETURN_CODE setAdCallType(VO_OSMP_AD_CALL_TYPE type);

        /**
         * Specify required UI settings for VAST/VMAP or IMA flow
         *
         * @param  settings [in] Required UI objects for VAST/VMAP or IMA flow. Refer to {@link VOOSMPAdUISettings}
         *
         * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
         */ 
        VO_OSMP_RETURN_CODE setUISettings(const VOOSMPAdUISettings& settings);

    private:
        VOOSMPString _adCallUrl;
        VO_OSMP_AD_CALL_TYPE _adCallType;
        VOOSMPAdUISettings _adUISettings;
};

#endif
