/************************************************************************
VisualOn Proprietary
Copyright (c) 2012-2016, VisualOn, Inc. All Rights Reserved
 
All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, Inc, and all rights
therein are expressly reserved. By accepting this material, the recipient
agrees that this material and the information contained therein are held
in confidence and in trust. The material may only be used and/or disclosed
as authorized in a license agreement controlling such use and disclosure.
************************************************************************/

#import <Foundation/Foundation.h>
#import "VOOSMPAudioRenderInfo.h"

/**
 * Protocol for audio render process. Implement this delegation when APP required to receive audio PCM data from the player.
 */
@protocol VOCommonPlayerAudioDelegate <NSObject>

/**
 * Returns estimated audio render latency in milliseconds.
 * This approach is for doing A/V sync solution.
 *
 * @return latency in milliseconds
 */
- (int) getLatency;

/**
 * Called when the audio data changed.
 *
 * @param audioFormat [in] Audio Format, specified as an {@link VOOSMPAudioFormat} object,
 *                    to be applied to cached audio format data before being returned.
 *
 */
- (void) onAudioFormatChanged:(nonnull id<VOOSMPAudioFormat>)audioFormat;

/**
 * Writes the audio data for playback.
 *
 * @param audioData the array that holds the data to play.
 * @param offsetInBytes the offset expressed in bytes in audioData where the data to write
 *    starts.
 *    Must not be negative, or cause the data access to go out of bounds of the array.
 * @param sizeInBytes the number of bytes to write in audioData after the offset.
 *    Must not be negative, or cause the data access to go out of bounds of the array.
 * @return zero or the positive number of bytes that were written. The number of bytes
 *    will be a multiple of the frame size in bytes not to exceed sizeInBytes.
 */
- (int) write:(nonnull id<VOOSMPAudioBuffer>)audioBuffer;

/**
 * Flushes the audio data currently queued for playback. Any data that has
 * been written but not yet presented will be discarded.
 */
- (void) flush;


@end
