/************************************************************************
VisualOn Proprietary
Copyright (c) 2012-2016, VisualOn, Inc. All Rights Reserved
 
All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, Inc, and all rights
therein are expressly reserved. By accepting this material, the recipient
agrees that this material and the information contained therein are held
in confidence and in trust. The material may only be used and/or disclosed
as authorized in a license agreement controlling such use and disclosure.
************************************************************************/


#import <Foundation/Foundation.h>


@protocol VOOSMPAudioFormat <NSObject>

/**
* The audio data encoding format.
*/
@property (readonly, assign) int audioFormat;

/**
* The audio data sample rate.
*/
@property (readonly, assign) int sampleRate;

/**
* The audio data channel.
*/
@property (readonly, assign) int channels;

/**
* The audio data sample bits.
*/
@property (readonly, assign) int sampleBits;

@end





@protocol VOOSMPAudioBuffer <NSObject>

/**
* Get the buffer timestamp.
*/
@property (readonly, assign) long long timeStamp;

/**
* Get the buffer size.
*/
@property (readonly, assign) int bufferSize;

/**
* Get the buffer data.
*/
@property (readonly, retain) NSData* pcmBuffer;

/**
* The offset bytes in audio buffer where the data to write starts.
*/
@property (readonly, assign) int offsetInBytes;

/**
* The bytes that were wrirtten by app.
*/
@property (assign) int wrirttenBytes;

@end
