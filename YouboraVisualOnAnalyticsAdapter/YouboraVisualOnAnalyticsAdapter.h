//
//  YouboraVisualOnAnalyticsAdapter.h
//  YouboraVisualOnAnalyticsAdapter
//
//  Created by Elisabet Massó on 29/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for YouboraVisualOnAnalyticsAdapter.
FOUNDATION_EXPORT double YouboraVisualOnAnalyticsAdapterVersionNumber;

//! Project version string for YouboraVisualOnAnalyticsAdapter.
FOUNDATION_EXPORT const unsigned char YouboraVisualOnAnalyticsAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraVisualOnAnalyticsAdapter/PublicHeader.h>


