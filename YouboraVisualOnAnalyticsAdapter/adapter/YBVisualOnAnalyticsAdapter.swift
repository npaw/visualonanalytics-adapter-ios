//
//  YBVisualOnAnalyticsAdapter.swift
//  YouboraVisualOnAnalyticsAdapter
//
//  Created by Elisabet Massó on 29/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraLib
import VisualOnOSMP

public protocol YBVisualOnAnalyticsAdapterDelegate {
    func onVOEvent(_ nID: VO_OSMP_CB_EVENT_ID, param1: Int32, param2: Int32, pObj: UnsafeMutableRawPointer!) -> VO_OSMP_RETURN_CODE
}

public class YBVisualOnAnalyticsAdapter: YBPlayerAdapter<AnyObject> {
    
    public var delegate: YBVisualOnAnalyticsAdapterDelegate?
    
    private var bitrate: Double?
    private var width: Int32?
    private var height: Int32?
    
    private var timer: Timer?

    private override init() {
        super.init()
    }
    
    public init(player: VOCommonPlayer, delegate: YBVisualOnAnalyticsAdapterDelegate?) {
        super.init(player: player)
        self.delegate = delegate
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? VOCommonPlayer {
            player.setOnEventDelegate(self)
        }
        monitorPlayhead(withBuffers: false, seeks: true, andInterval: 800)
        resetValues()
    }
    
    public override func unregisterListeners() {
        monitor?.stop()
        resetValues()
        super.unregisterListeners()
    }
    
    func resetValues() {
        bitrate = nil
        width = nil
        height = nil
        timer?.invalidate()
        timer = nil
    }
    
    // MARK: - Getters
    
    public override func getBitrate() -> NSNumber? {
        guard let bitrate = bitrate else { return super.getBitrate() }
        return NSNumber(value: bitrate)
    }
    
    public override func getDuration() -> NSNumber? {
        guard let player = player as? VOCommonPlayer else { return super.getDuration() }
        return NSNumber(value: milisToSeconds(player.getDuration()))
    }
    
    public override func getIsLive() -> NSValue? {
        guard let player = player as? VOCommonPlayer else { return super.getIsLive() }
        return NSNumber(value: player.isLiveStreaming())
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let player = player as? VOCommonPlayer else { return super.getPlayhead() }
        return NSNumber(value: milisToSeconds(player.getPosition()))
    }
    
    public override func getRendition() -> String? {
        guard let width = width, let height = height, let bitrate = bitrate else { return super.getRendition() }
        return YBYouboraUtils.buildRenditionString(withWidth: width, height: height, andBitrate: bitrate)
    }
    
    public override func fireStop() {
        resetValues()
        super.fireStop()
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdapterVersion()
    }
    
}

extension YBVisualOnAnalyticsAdapter {
    
    private enum Properties: String {
        case description
        case language
        case codec
        case bitrate
        case width
        case height
        case type
        case channelcount
    }
    
    private func setTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(getPlayerInfo), userInfo: nil, repeats: true)
    }
    
    @objc
    private func getPlayerInfo() {
        guard let player = player as? VOCommonPlayer, let asset = player.getPlayingAsset(), let videoProperties = player.getVideoProperty(asset.videoIndex), let audioProperties = player.getAudioProperty(asset.audioIndex) else { return }
        
        var videoBitrate = 0.0
        var audioBitrate = 0.0
        
        for property in 0..<videoProperties.getCount() {
            var videoValue: Any?
            if let videoKeyString = videoProperties.getKey(property), let videoKey = Properties(rawValue: videoKeyString) {
                videoValue = videoProperties.getValue(property)
                if videoKey == .bitrate, let value = videoValue as? String {
                    videoBitrate = Double(value.split(separator: " ")[0]) ?? 0
                }
            }
        }
        
        for property in 0..<audioProperties.getCount() {
            var audioValue: Any?
            if let audioKeyString = audioProperties.getKey(property), let audioKey = Properties(rawValue: audioKeyString) {
                audioValue = audioProperties.getValue(property)
                if audioKey == .bitrate, let value = audioValue as? String {
                    audioBitrate = Double(value.split(separator: " ")[0]) ?? 0
                }
            }
        }
        
        bitrate = videoBitrate + audioBitrate
        
    }
    
    private func milisToSeconds(_ value: Int) -> Int {
        return value / 1000
    }
    
}

extension YBVisualOnAnalyticsAdapter: VOCommonPlayerDelegate {
    
    public func onVOEvent(_ nID: VO_OSMP_CB_EVENT_ID, param1: Int32, param2: Int32, pObj: UnsafeMutableRawPointer!) -> VO_OSMP_RETURN_CODE {
        guard let player = player else { return delegate?.onVOEvent(nID, param1: param1, param2: param2, pObj: pObj) ?? VO_OSMP_ERR_NONE }
        switch nID {
            case VO_OSMP_CB_ERROR:
                fireError(withMessage: nil, code: nil, andMetadata: nil)
            case VO_OSMP_SRC_CB_OPEN_FINISHED,
                 VO_OSMP_SRC_CB_DOWNLOAD_FAIL:
                let errorCode = VO_OSMP_RETURN_CODE(UInt32(truncatingIfNeeded: param1))
                if errorCode != VO_OSMP_ERR_NONE {
                    // OPEN_FAILED
                    fireFatalError(withMessage: nil, code: "\(errorCode)", andMetadata: nil)
                }
            case VO_OSMP_CB_PLAY_COMPLETE:
                if flags.joined {
                    fireStop()
                }
            case VO_OSMP_CB_VIDEO_START_BUFFER,
                 VO_OSMP_CB_AUDIO_START_BUFFER:
                fireBufferBegin()
                break
            case VO_OSMP_CB_VIDEO_STOP_BUFFER,
                 VO_OSMP_CB_AUDIO_STOP_BUFFER:
                fireBufferEnd()
                break
            case VO_OSMP_CB_VIDEO_SIZE_CHANGED:
                width = param1
                height = param2
            case VO_OSMP_CB_PLAYSTATE_CHANGED:
                let state = PlayerState(rawValue: player.getPlayerState().rawValue)
                if state == .opened {
                    if flags.joined {
                        fireStop()
                    }
                    setTimer()
                    fireStart() // TODO: Check
                } else if state == .playing {
                    fireResume()
                    fireStart()
                    fireJoin()
                } else if state == .paused {
                    firePause()
                } else if state == .stopped || state == .suspended || state == .destroyed {
                    fireStop()
                }
                break
            default:
                break
        }
        return delegate?.onVOEvent(nID, param1: param1, param2: param2, pObj: pObj) ?? VO_OSMP_ERR_NONE
    }
    
    private enum PlayerState: UInt32 {
        case uninitialized
        case initialized
        case opening
        case opened
        case playing
        case paused
        case stopped
        case suspended
        case destroyed
        case max
    }
    
}
