//
//  PlayerViewController.swift
//  VOAnalyticsSample
//
//  Created by Elisabet Massó on 29/11/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import YouboraLib
import YouboraConfigUtils
import YouboraVisualOnAnalyticsAdapter
import VisualOnOSMP

class PlayerViewController: UIViewController {
    
    var plugin: YBPlugin?
    
    var resource: String?
    
    var player: VOCommonPlayer?
    
    var timer: Timer?
    
    var isTouchingSlider = false
    
    var changeItemButton = UIButton(type: .custom)
    var buttonReplay = UIButton(type: .custom)
    var playerContainer = UIView()
    
    private let controlBtn: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "pause")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleControls), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private let leftTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let currentTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let videoSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.red
        slider.maximumTrackTintColor = UIColor.white
        slider.thumbTintColor = UIColor.red
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(handleSliderValueChanged), for: [.touchUpInside, .touchDown])
        slider.isHidden = true
        return slider
    }()
    
    private let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let indicator: UIActivityIndicatorView = {
        let uiIndicator = UIActivityIndicatorView()
        uiIndicator.hidesWhenStopped = true
        uiIndicator.color = .white
        return uiIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black
        
        let options = YouboraConfigManager.getOptions()
        plugin = YBPlugin(options: options)
        
        // Initialize player on this view controller
        initializePlayer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            timer?.invalidate()
            timer = nil
            player?.stop()
            player?.close()
            player = nil
            plugin?.fireStop()
            plugin?.removeAdapter()
        }
        
    }
    
    private func configureControls() {
        view.backgroundColor = .black
        
        changeItemButton.setTitle("Change Item", for: .normal)
        changeItemButton.addTarget(self, action: #selector(changeItem), for:.touchUpInside)
        changeItemButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(changeItemButton)
        NSLayoutConstraint.activate([
            changeItemButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            changeItemButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            changeItemButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            changeItemButton.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        buttonReplay.setTitle("Replay", for: .normal)
        buttonReplay.addTarget(self, action: #selector(pressToReplay), for:.touchUpInside)
        buttonReplay.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonReplay)
        NSLayoutConstraint.activate([
            buttonReplay.topAnchor.constraint(equalTo: changeItemButton.bottomAnchor, constant: 20),
            buttonReplay.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            buttonReplay.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            buttonReplay.heightAnchor.constraint(equalToConstant: 20)
        ])
        buttonReplay.isHidden = true
        
        playerContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(playerContainer)
        NSLayoutConstraint.activate([
            playerContainer.topAnchor.constraint(equalTo: buttonReplay.bottomAnchor, constant: 0),
            playerContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            playerContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            playerContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])

        container.frame = view.bounds
        view.addSubview(container)
        container.topAnchor.constraint(equalTo: playerContainer.topAnchor, constant: 0).isActive = true
        container.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        container.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 9/16).isActive = true
        
        container.addSubview(indicator)
        container.bringSubviewToFront(indicator)
        indicator.center = container.center
        
        controlBtn.frame = container.bounds
        controlBtn.bounds = controlBtn.frame.insetBy(dx: 20.0, dy: 20.0)
        container.addSubview(controlBtn)
        controlBtn.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10).isActive = true
        controlBtn.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8).isActive = true
        controlBtn.widthAnchor.constraint(equalToConstant: 15).isActive = true
        controlBtn.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
//        if let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) {
            buttonReplay.isHidden = false
            leftTimeLbl.frame = container.bounds
            container.addSubview(leftTimeLbl)
            leftTimeLbl.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -10).isActive = true
            leftTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            leftTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            leftTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            leftTimeLbl.isHidden = false

            currentTimeLbl.frame = container.bounds
            container.addSubview(currentTimeLbl)
            currentTimeLbl.leftAnchor.constraint(equalTo: controlBtn.rightAnchor, constant: 10).isActive = true
            currentTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            currentTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            currentTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            currentTimeLbl.isHidden = false

            videoSlider.frame = container.bounds
            container.addSubview(videoSlider)
            videoSlider.rightAnchor.constraint(equalTo: leftTimeLbl.leftAnchor).isActive = true
            videoSlider.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            videoSlider.leftAnchor.constraint(equalTo: currentTimeLbl.rightAnchor).isActive = true
            videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
            videoSlider.isHidden = false
//        }
        
        view.layoutIfNeeded()
    }
    
    @objc
    private func handleControls() {
        guard let player = player else { return }
        let state = PlayerState(rawValue: player.getPlayerState().rawValue)
        if state == .paused {
            player.start()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        } else if state == .playing {
            player.pause()
            controlBtn.setImage(UIImage(named: "play"), for: .normal)
        } else {
            pressToReplay()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    @objc
    private func handleSliderValueChanged() {
        if let totalSeconds = player?.getDuration().msToSeconds {
            let value = Double(videoSlider.value) * totalSeconds
            player?.setPosition(Int(value).secToMilis)
            isTouchingSlider = !isTouchingSlider
        }
    }
    
    @objc
    func handleTime() {
        if let currentTime = player?.getPosition().msToSeconds, let duration = player?.getDuration().msToSeconds {
            currentTimeLbl.text = "\(getTextTimeFrom(time: Double(currentTime)))"
            leftTimeLbl.text = "-\(getTextTimeFrom(time: Double(duration - currentTime)))"
            if !isTouchingSlider {
                videoSlider.value = Float(currentTime / duration)
            }
        }
    }
    
    @objc
    func changeItem() {
        guard let newResource = ["https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"].randomElement() else {
            return
        }
        resource = newResource
        plugin?.options.contentIsLive = NSNumber(false)
        
        // Change content in player
        player?.stop()
        player?.close()
        open(with: newResource)
        
        configureControls()
    }
    
    @objc
    func pressToReplay() {
        if let player = player {
            let state = PlayerState(rawValue: player.getPlayerState().rawValue)
            if let resource = resource, state != .playing && state != .paused {
                open(with: resource)
                return
            }
        }
        player?.setPosition(0)
        player?.start()
    }
    
    private func getTextTimeFrom(time totalSeconds: Double) -> String {
        if !totalSeconds.isNaN && !totalSeconds.isInfinite {
            let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
            let secondsText = String(format: "%02d", seconds)
            let minutesText = String(format: "%02d", Int(totalSeconds / 60))
            return "\(minutesText):\(secondsText)"
        }
        return ""
    }
    
    private enum PlayerState: UInt32 {
        case uninitialized
        case initialized
        case opening
        case opened
        case playing
        case paused
        case stopped
        case suspended
        case destroyed
        case max
    }

}

extension PlayerViewController {
    
    private func initializePlayer() {
        
        player = VOCommonPlayerImpl(VO_OSMP_VOME2_PLAYER, initParam: nil)
        
        let licensePath = Bundle.main.path(forResource: "voVidDec", ofType: "dat")
        
        guard player?.setLicenseFilePath(licensePath) == VO_OSMP_ERR_NONE else {
            print("Set license file path failed")
            return
        }
        
        guard player?.setPreAgreedLicense("ABCDE") == VO_OSMP_ERR_NONE else {
            print("Set pre agreed license failed")
            return
        }
        
        guard let player = player else {
            print("Player is nil")
            return
        }
        
        let rect = OCFunction.rect()
        
        player.setZoom(VO_OSMP_ZOOM_LETTERBOX, rect: rect);
        
//        player.setOnEventDelegate(self)
        
        configureControls()
        
        // Set view
        let unmanaged = Unmanaged.passUnretained(playerContainer)
        let viewPointer = UnsafeMutableRawPointer(unmanaged.toOpaque())
        player.setView(viewPointer)
        
        plugin?.adapter = YBVisualOnAnalyticsAdapter(player: player, delegate: self)
        
        // Start playback
        play()
        
    }
    
    func play() {
        guard let resource = resource, let player = player else {
            print("Player or resource is empty")
            return
        }
        
        plugin?.options.contentResource = resource
        
        // Set up DRM
        player.setDRMLibrary(nil, libApiName: nil)
                
        open(with: resource)
    }
    
    func open(with resource: String) {
        // Open param
        let openParam = VOOSMPOpenParam()
        openParam?.decoderType = Int32(VO_OSMP_DEC_VIDEO_SW.rawValue | VO_OSMP_DEC_AUDIO_SW.rawValue)
        
        // Start player
        player?.open(resource, flag: VO_OSMP_FLAG_SRC_OPEN_ASYNC, sourceType: VO_OSMP_SRC_AUTO_DETECT, openParam: openParam)
    }
    
    func stop() {
        controlBtn.setImage(UIImage(named: "resume"), for: .normal)
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        player?.stop()
        player?.close()
    }
    
}

extension PlayerViewController: YBVisualOnAnalyticsAdapterDelegate {
    
    func onVOEvent(_ nID: VO_OSMP_CB_EVENT_ID, param1: Int32, param2: Int32, pObj: UnsafeMutableRawPointer!) -> VO_OSMP_RETURN_CODE {
        switch nID {
            case VO_OSMP_CB_PLAY_COMPLETE:
                stop()
            case VO_OSMP_SRC_CB_OPEN_FINISHED:
                let ret = VO_OSMP_RETURN_CODE(UInt32(truncatingIfNeeded: param1))
                if ret != VO_OSMP_ERR_NONE {
                    print("\(ret)")
                    // OPEN_FAILED
                } else {
                    player?.start()
                    // OPEN_FINISHED
                    controlBtn.isHidden = false
                    if timer == nil {
                        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(handleTime), userInfo: nil, repeats: true)
                    }
                }
            default:
                break
        }
        return VO_OSMP_ERR_NONE
    }
    
}

extension Int {
    var msToSeconds: Double { Double(self) / 1000 }
    var secToMilis: Int { self * 1000 }
}
