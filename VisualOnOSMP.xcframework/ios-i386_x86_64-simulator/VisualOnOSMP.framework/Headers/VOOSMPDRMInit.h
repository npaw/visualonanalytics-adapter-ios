/************************************************************************
 VisualOn Proprietary
 Copyright (c) 2019, VisualOn Incorporated. All Rights Reserved
 
 VisualOn, Inc., USA
 
 All data and information contained in or disclosed by this document are
 confidential and proprietary information of VisualOn, and all rights
 therein are expressly reserved. By accepting this material, the
 recipient agrees that this material and the information contained
 therein are held in confidence and in trust. The material may only be
 used and/or disclosed as authorized in a license agreement controlling
 such use and disclosure.
 ************************************************************************/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VOOSMPDRMInit : NSObject

/*
 * DRM Init size.
 */
@property (nonatomic, assign)int size;

/*
 * DRM Init data.
 */
@property (nonatomic, assign)Byte *data;

/*
 * DRM Init data handle.
 */
@property (nonatomic, assign)long dataHandle;

/*
 * DRM Thirdparty function set pointer.
 */
@property (nonatomic, assign)long thirdPartyFunctionSet;

@end

NS_ASSUME_NONNULL_END
